<?php

add_shortcode( 'sabisu-carrousel', 'sabisu_carrousel_function' );

function colibri_sabisu_init(){
    function sabisu_carrousel_function( $atts, $content, $tag ) {
        $url = get_stylesheet_directory_uri();
        return "<div class='sabisu-carrousel-margin'>
        <div id='sabisu-carrousel-container'>
            <div class='item' id='first_img'>
                <a href='https://sabisu.com.mx/saf-tehnika/'>
                    <img src='$url/img/SAF.png'>
                </a>
            </div>
            <div class='item'>
                <a href='https://sabisu.com.mx/rajant/'>
                    <img src='$url/img/RAJANT.png'>
                </a>
            </div>
            <div class='item'>
                <a href='https://sabisu.com.mx/aranet/'>
                    <img src='$url/img/ARANET.png'>
                </a>
            </div>
            <div class='item'>
                <a href='https://sabisu.com.mx/albentia/'>
                    <img src='$url/img/WISP.png'>
                </a>
            </div>
            <div class='item item-extra-width'>
                <a href='https://sabisu.com.mx/racom/'>
                    <img src='$url/img/RACOM.png'>
                </a>
            </div>
            <div class='item'>
                <a href='https://sabisu.com.mx/soporte/'>
                    <img src='$url/img/SERVICIOS.png'>
                </a>
            </div>
            <div class='center'>
                <div class='wrapper'>
                    <div class='wrapper-img-container'>
                        <img src='$url/screenshot.png' style='z-index: 100'>
                    </div>
                    <div class='wrapper-img-container2'>
                        <img class='img-back' src='$url/img/gear.png'>
                    </div>
                </div>
            </div>
        </div>
        </div>
        ";
    }
}

add_action('init', 'colibri_sabisu_init');


function add_theme_scripts() {
    wp_enqueue_style ('theme-style', get_stylesheet_directory_uri().'/css/carrousel.css');
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/carrousel.js', array ( 'jquery' ), 1.1, true);
    // wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/carrousel.js');
}
add_action ( 'wp_enqueue_scripts', 'add_theme_scripts' );

// Create an options page to display the list of shortcodes in WordPress
function diwp_display_shortcodes_admin_page() {
    add_options_page('All Active Shortcodes', 'Active Shortcodes', 'manage_options', 'active-shortcodes', 'diwp_display_list_of_shortcodes');
}

add_action('admin_menu', 'diwp_display_shortcodes_admin_page'); 

// function to display the list of active shortcodes in WordPress

function diwp_display_list_of_shortcodes(){
    global $shortcode_tags;
    
    $shortcodes = $shortcode_tags;
    
    // sort the shortcodes with alphabetical order
    ksort($shortcodes);
    
    echo "<h2>List of All Active Shortcodes in Your Website.</h2>";

    echo "<ol>";
    
    foreach ($shortcodes as $shortcode => $value) {
        echo '<li>['.$shortcode.']</li>';
    }
    echo "</ol>";
}

?>