<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sabisute_wp53' );

/** MySQL database username */
// define( 'DB_USER', 'sabisute_wp53' );
define( 'DB_USER', 'root' );

/** MySQL database password */
// define( 'DB_PASSWORD', 'p62wS5[2-k' );
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WP_HOME','http://sabisu.test/sabisu');
define('WP_SITEURL','http://sabisu.test/sabisu');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xjvdzzsl0hcd1exnsh5qqpabfelagpw21xwdfsjco3tvkskefkqtpamrpazbwzvc' );
define( 'SECURE_AUTH_KEY',  '5eozt1rrkibetq54bkzpy7zo7ljsoaadosuezdcew3mjjemvstdzzxgylqzgrts1' );
define( 'LOGGED_IN_KEY',    'uojvxatfeujmbip8fjnpc5smapysjqfogxe4fg2atuiv3ymborme5lufzj1phcbt' );
define( 'NONCE_KEY',        'ehzft6mw7yj68uhtgsrgimcz2dylb1gzaumiqjdfz92oaawqd1lysflfrk9yspyp' );
define( 'AUTH_SALT',        'jw1d3zzwlzqafto9eilrr3f5umtqbckequz4xttvz2ca7fxrdwrxwokcfipfmsnl' );
define( 'SECURE_AUTH_SALT', '0ozlm0en2sem2shptftvjlhdxndzwnwmjfgsuxnattc8rzpcgthhfaxu8dxlggjt' );
define( 'LOGGED_IN_SALT',   'gh3bvyxymsbxebcnvyklkdblyzzd6brlrcbuorqpgo99vsgm9i8xjhbwcepnhw4a' );
define( 'NONCE_SALT',       'etcmkxu4dji869lo4cmaxdlhrsmpqemek8ll0afgzfuav9lm8tojngpemojwwbax' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpr2_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_MEMORY_LIMIT', '512M' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
