var radius = 200; // adjust to move out items in and out
var angle = 0;
var fields = null;
var step = null;

// window.onload = function () {
    // this is the radius circle
    angle = 0;
    fields = jQuery('.item'),
    container = jQuery('#sabisu-carrousel-container'),
    width = container.width(),
    height = container.height();
    step = (2 * Math.PI) / fields.length;
    radius = width / 2; // adjust to move out items in and out
    setAngles();

    jQuery(".item").hover(function () {
        jQuery("#sabisu-carrousel-container").addClass('paused');
        jQuery(".item,.wrapper-img-container,.wrapper-img-container2").addClass('paused');
    }, function () {
        jQuery("#sabisu-carrousel-container").removeClass('paused');
        jQuery(".item,.wrapper-img-container,.wrapper-img-container2").removeClass('paused');
    });
// };

window.onresize = function () {
    angle = 0;
    fields = jQuery('.item'),
    container = jQuery('#sabisu-carrousel-container'),
    width = container.width(),
    height = container.height();
    console.log(width);
    step = (2 * Math.PI) / fields.length;
    radius = width / 2; // adjust to move out items in and out
    setAngles();
}

window.onbeforeunload = function(event)
{
    jQuery("#sabisu-carrousel-container").addClass('paused');
    jQuery(".item,.wrapper-img-container,.wrapper-img-container2").addClass('paused');
    return null;
};

function setAngles() {

    fields.each(function () {
        var x = Math.round(width / 2 + radius * Math.cos(angle) - jQuery(this).width() / 2);
        var y = Math.round(height / 2 + radius * Math.sin(angle) - jQuery(this).height() / 2);
        jQuery(this).css({
            left: x + 'px',
            top: y + 'px'
        });
        angle += step;
    });
}